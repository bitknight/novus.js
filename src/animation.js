import PIXI from 'pixi.js';

// Animation factory
// Provide some basic animation functionality using PIXI sprites and textures

function animation(opts) {
  // Destruct
  let {
    texture: texture = null,
    frames: frames = [],
    fps: fps = 60,
    width: width = 0,
    height: height = 0,
    loop: loop = false
  } = opts;

  let sprite = new PIXI.Sprite()
    , elapsed = 0
    , limit = 1 / fps
    , totalFrames = frames.length
    , textures = []
    , idx = 0
    , paused = false;

  // Store textures
  for (let i = 0; i < frames.length; ++i) {
    textures.push(new PIXI.Texture(texture, new PIXI.Rectangle(frames[i] * width, 0, width, height)));
  }

  sprite.texture = textures[idx];

  return {
    sprite: sprite,

    update(dt) {
      if (!paused) {
        elapsed += dt;
      }

      if (elapsed >= limit) {
        elapsed = 0;
        ++idx;
        sprite.texture = textures[idx % textures.length];
      }
    },

    stop() {
      paused = true;
    },

    play(from = 0) {
      paused = false;
      if (from < frames.length) {
        idx = from;
      }
    }
  };
}

export default animation;
