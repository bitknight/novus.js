import PIXI from 'pixi.js';
import animation from 'src/animation';

console.log(PIXI.VERSION);
window.PIXI = PIXI;

PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;

var renderer = new PIXI.autoDetectRenderer(800, 600, {
	antialias: false,
	backgroundColor: '0x000000'
});

document.body.appendChild(renderer.view);

var astroSheet = PIXI.Texture.fromImage('../assets/astro.png');

var anim = animation({
	texture: astroSheet,
	frames: [1, 2, 3, 4, 5, 6],
	fps: 10,
	width: 10,
	height: 10,
	loop: true
});

anim.sprite.anchor.x = 0.5;
anim.sprite.anchor.y = 0.5;
// relative to container
anim.sprite.position.x = 0;
anim.sprite.position.y = 0;
anim.sprite.scale.set(4, 4);

var camera = new PIXI.Container();

// center camera
camera.position.x = 400;
camera.position.y = 300;

camera.addChild(anim.sprite);

requestAnimationFrame(update);

function update (hrt) {
	requestAnimationFrame(update);

	anim.update(1/60);

	renderer.render(camera);
}
